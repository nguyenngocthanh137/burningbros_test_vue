import { DataRequest } from "../../type";
import axios from "axios";

export const getProductApi = async (data: DataRequest) => {
    try {
        const res = await axios.get('/products/search', { params: { limit: data?.limit, q: data?.search } })
        return res?.data
    }
    catch (error) {
        console.log(error)
    }
}