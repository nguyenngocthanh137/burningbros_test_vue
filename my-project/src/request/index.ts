import axios from "axios";

const BASE_URL = import.meta.env.VITE_API_URL as string;

axios.defaults.baseURL = BASE_URL

export default axios
